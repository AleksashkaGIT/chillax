package com.samsolutions.chillax;
public class HelloWorld {
    private String message;
    public void setMessage(String message) {
        this.message = message;
    }
    public String getMessage(){
        return message;
    }
	public String toString(){
		return "Hello, " + message + "!";
	}
}
