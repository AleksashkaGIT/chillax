<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE HTML5>
<style type="text/css">
    input[type="text"]:invalid:not(:placeholder-shown){border:2px solid red;}
</style>
<html>
<head>
    <title>Chillax</title>
</head>
<body>
<h2>Please, enter your name:</h2>
<form action = "servlet"  method = "GET">
	<input type = "text" name="name" pattern = "[A-Za-zА-Яа-яЁё]{2,}" oninvalid="this.setCustomValidity('Please, input correct name!')" oninput="setCustomValidity('')" placeholder="Your name" required><br>
    <input type="submit" name ="button" value="Input">
</form>
<h1><% if (request.getAttribute("greeting") != null) {
        out.print(request.getAttribute("greeting"));
	} %></h1>
</body>
</html>
